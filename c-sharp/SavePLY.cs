static void SavePLY(string filename, ref PointCloud cloud)
{
    float[,,] points = cloud.ToArray();
    int rows = points.GetLength(0);
    int cols = points.GetLength(1);
    int size = rows * cols;
 
    StreamWriter sw = new StreamWriter(File.Open(filename, FileMode.Create));
    sw.WriteLine("ply");
    sw.WriteLine("format binary_little_endian 1.0");
    sw.WriteLine("element vertex {0}", size);
    sw.WriteLine("property float x");
    sw.WriteLine("property float y");
    sw.WriteLine("property float z");
    sw.WriteLine("property uchar red");
    sw.WriteLine("property uchar green");
    sw.WriteLine("property uchar blue");
    sw.WriteLine("end_header");
    sw.Close();
 
    BinaryWriter bw = new BinaryWriter(File.Open(filename, FileMode.Append));
    for (int i = 0; i < points.GetLength(0); i++)
    {
        for (int j = 0; j < points.GetLength(1); j++)
        {
            float x = points[i, j, 0];
            float y = points[i, j, 1];
            float z = points[i, j, 2];
            byte r = (byte)points[i, j, 4];
            byte g = (byte)points[i, j, 5];
            byte b = (byte)points[i, j, 6];
 
            // Filter away Not-a-Number points (comment this out if this behavior is not desired)
            if (Single.IsNaN(x) || Single.IsNaN(y) || Single.IsNaN(z))
                x = y = z = 0;
 
            bw.Write(x);
            bw.Write(y);
            bw.Write(z);
            bw.Write(r);
            bw.Write(g);
            bw.Write(b);
        }
    }
    bw.Close();
}